import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'util.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
    theme: ThemeData(
      hintColor: Colors.red,
      primaryColor: colorMain,
    ),
    debugShowCheckedModeBanner: false,
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _lista = [];

  @override
  void initState() {
    super.initState();
    _lerDados().then((dados) {
      setState(() {
        _lista = json.decode(dados);
      });
    });
  }

  final _tarefaControle = TextEditingController();

  Future<void> _atualizar() async {
    // await Future.delayed(Duration(seconds: 1));
    setState(() {
      _lista.sort((a, b) {
        if (a['ok'] && !b['ok'])
          return 1;
        else if (!a['ok'] && b['ok'])
          return -1;
        else
          return 0;
      });
      _salvarDados();
    });
  }

  void _addTarefa() {
    if (_tarefaControle.text != '') {
      setState(() {
        Map<String, dynamic> novaTarefa = Map();
        novaTarefa['titulo'] = _tarefaControle.text;
        novaTarefa['ok'] = false;
        _tarefaControle.text = '';
        _lista.add(novaTarefa);
        _salvarDados();
        FocusScope.of(context).requestFocus(new FocusNode()); //abaixar teclado
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _atualizar,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Lista de tarefas",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: colorMain,
        ),
        body: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextFormField(
                      controller: _tarefaControle,
                      cursorColor: colorMain,
                      style: TextStyle(fontSize: 18),
                      onFieldSubmitted: (value) {
                        _addTarefa();
                      },
                      decoration: InputDecoration(
                          labelText: "Nova tarefa",
                          labelStyle: TextStyle(
                            color: colorMain,
                          )),
                    ),
                  ),
                  RaisedButton(
                    color: colorMain,
                    child: Text(
                      "ADD",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: _addTarefa,
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _lista.length,
                itemBuilder: item,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget item(context, index) {
    return Dismissible(
      // key: Key(index.toString()),
      key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
      direction: DismissDirection.startToEnd,
      background: Container(
          color: Colors.red[300],
          child: Align(
            alignment: Alignment(-0.9, 0.0),
            child: Icon(
              Icons.delete,
              color: Colors.white,
            ),
          )),
      child: CheckboxListTile(
        title: Text(_lista[index]["titulo"]),
        value: _lista[index]["ok"],
        secondary: CircleAvatar(
          child: Icon(
            _lista[index]["ok"] ? Icons.check_circle : Icons.error,
            color: colorMain,
            size: 40,
          ),
          backgroundColor: Colors.white,
        ),
        onChanged: (bool marcar) {
          setState(() {
            _lista[index]["ok"] = marcar;
            _salvarDados();
          });
        },
      ),
      onDismissed: (direction) {
        setState(() {
          Map<String, dynamic> tarefaDeletada = Map();
          tarefaDeletada = Map.from(_lista[index]);
          // tarefaDeletada['titulo'] = _lista[index]['titulo'];
          // tarefaDeletada['ok'] = _lista[index]['ok'];

          int tarefaDeletadaPosicao = index;

          _lista.removeAt(index);
          _salvarDados();

          final snack = SnackBar(
            content: Text("Tarefa \"${tarefaDeletada['titulo']}\" removida!"),
            duration: Duration(seconds: 5),
            backgroundColor: colorMain,
            action: SnackBarAction(
              textColor: Colors.red,
              label: 'Desfazer',
              onPressed: () {
                setState(() {
                  _lista.insert(tarefaDeletadaPosicao, tarefaDeletada);
                  _salvarDados();
                });
              },
            ),
          );

          Scaffold.of(context).showSnackBar(snack);
        });
      },
    );
  }

  Future<File> _obterArquivo() async {
    final diretorio = await getApplicationDocumentsDirectory();
    return File("${diretorio.path}/dados.json");
  }

  Future<File> _salvarDados() async {
    String dados = json.encode(_lista);
    final arquivo = await _obterArquivo();
    return arquivo.writeAsString(dados);
  }

  Future<String> _lerDados() async {
    try {
      final arquivo = await _obterArquivo();
      return arquivo.readAsString();
    } catch (e) {
      return null;
    }
  }
}
